let csrfToken = $("[name=csrfmiddlewaretoken]").val();


function deleteCharacter(item) {
  var characterId = $(item).val();
  if (confirm("Do you really want to delete this character?")) {
    $.ajax({
      url: '/main/delete_character',
      type: 'POST',
      headers: {
             'X-CSRFToken': csrfToken
           },
      data: {
        character_id: characterId,
      }
    })
    .done(function(response) {
      $('.selection-box').html(response)
    });
  }
}
