let csrfToken = $("[name=csrfmiddlewaretoken]").val();

function cardOnClick(card) {
  if (card.parent().attr("id") == 'hand') {
    var isIn = 'hand';
  } else if (card.parent().attr("id") == 'ground') {
    var isIn = 'ground';
  }
  cardClick(isIn, card);
};

// $('.card').on('click', function(event) {
//   if ($(this).parent().attr("id") == 'hand') {
//     var isIn = 'hand';
//   } else if ($(this).parent().attr("id") == 'ground') {
//     var isIn = 'ground';
//   }
//   $.fn.cardClick(isIn, $(this));
// });

function pickUpCard(card) {
  if ($('#hand').hasClass('slidedown')) {
    card.addClass('slideright');
  } else {
    card.css('left', '-100vw');
  }
  card.prependTo('#hand');
}

function discardCard(card) {
  card.css('left', '0');
  card.remove();
}


function cardClick(isIn, card) {
  $.ajax({
    url: '/main/card_click',
    type: 'POST',
    headers: {
           'X-CSRFToken': csrfToken
         },
    data: {
      character_id: characterId,
      card_id: card.find('.card-id').text(),
      is_in: isIn
    }
  })
  .done(function(response) {
    if (response['allowed']) {
      if (response['command'] == 'pick-up') {
        pickUpCard(card);
      } else if (response['command'] == 'discard') {
        discardCard(card);
      }
      $("#character").html(response.render);
    }
    $('#action-box p').text(response.msg);
  });
}


function move() {
  $.ajax({
    url: '/main/move',
    type: 'POST',
    headers: {
           'X-CSRFToken': csrfToken
         },
    data: {
      character_id: characterId
    }
  })
  .done(function(response) {
    $('#character').html(response.character);
    $('#hand').html(response.hand);
    $('#ground').html(response.ground);
    var handCards = $('#hand').find('.card');
    var groundCards = $('#ground').find('.card');
    handCards.addClass('slideright');
    $(handCards).on('click', function(event) {
      cardOnClick($(this));
    });
    $(groundCards).on('click', function(event) {
      cardOnClick($(this));
    });
    $('#action-box p').text(response.msg);
  });
}


$('.card').on('click', function(event) {
  cardOnClick($(this));
});


$('.hand-tab').on('click', function(event) {
  var hand = $('#hand');
  var cards = hand.find('.card');
  var arrow = $(this).find('i');

  if (hand.hasClass('slideup')) {
    hand.removeClass('slideup');
    hand.addClass('slidedown');
    hand.one("animationend", function() {
      cards.removeClass('slideleft');
      cards.addClass('slideright');
    });
    arrow.toggleClass("fa-chevron-down fa-chevron-up");
  } else if (hand.hasClass('slidedown')) {
    hand.css('height', '24em');
    cards.removeClass('slideright');
    cards.addClass('slideleft');
    if (!hand.lenght){
      hand.removeClass('slidedown');
      hand.addClass('slideup');
    } else {
      cards.last().one("animationend", function() {
        hand.removeClass('slidedown');
        hand.addClass('slideup');
      });
    }
    arrow.toggleClass("fa-chevron-down fa-chevron-up");
  } else {
    hand.addClass('slidedown');
    hand.one("animationend", function() {
      cards.addClass('slideright');
    });
    arrow.toggleClass("fa-chevron-down fa-chevron-up");
  }
});
