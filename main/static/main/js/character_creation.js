$(document).ready(function (){
  var str = parseInt($('#id_strenght').val());
  var dex = parseInt($('#id_dexterity').val());
  var int = parseInt($('#id_intellect').val());
  $('#pts-remain').text(10 - (str + dex + int));
});


function incPts(button) {
  var characterAttr = $(button).parent().find('input');
  var ptsRemain = $('#pts-remain')
  if (parseInt(ptsRemain.text()) > 0) {
    characterAttr.val(parseInt(characterAttr.val())+1);
    ptsRemain.text(parseInt(ptsRemain.text())-1);
  }
}


function decPts(button) {
  var characterAttr = $(button).parent().find('input');
  var ptsRemain = $('#pts-remain')
  if (parseInt(characterAttr.val()) > 1) {
    characterAttr.val(parseInt(characterAttr.val())-1);
    ptsRemain.text(parseInt(ptsRemain.text())+1);
  }
}
