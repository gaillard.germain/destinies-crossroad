from django.db import models
from registration.models import User
from random import randint, choice


class Category(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Effect(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Card(models.Model):
    name = models.CharField(max_length=50)
    category = models.ForeignKey(
        Category, related_name='cards',
        on_delete=models.SET_NULL, null=True
    )
    value = models.IntegerField()
    image = models.ImageField(upload_to='cards/', blank=True, null=True)
    description = models.CharField(max_length=200, blank=True, null=True)
    cumulative = models.BooleanField(default=False)
    enduring = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    def play(self, character, is_in):
        """ Plays a card, returns a dict as response """

        response = {}
        character_card = character.cards.filter(card=self).first()

        if is_in == 'hand':
            character_card.on_hand -= 1
            character_card.on_discard += 1
            character_card.save()

            response['allowed'] = True
            response['command'] = 'discard'
            response['msg'] = "{} removed from {}'s hand".\
                format(self.name.title(), character.firstname.title())

        elif is_in == 'ground':
            if self.cumulative:
                same_cat = False
            else:
                same_cat = character.cards.\
                    filter(card__category=self.category).\
                    filter(on_hand__gt=0).count()
            if same_cat:
                response['allowed'] = False
                response['msg'] = "{} already has {} card".\
                    format(character.firstname.title(),
                           self.category.__str__().title())
            else:
                character_card.on_hand += 1
                character_card.on_ground -= 1
                character_card.save()

                response['allowed'] = True
                response['command'] = 'pick-up'
                response['msg'] = "{} added to {}'s hand".\
                    format(self.name.title(), character.firstname.title())

        return response


class CardEffect(models.Model):
    card = models.ForeignKey(
        Card, related_name='effects',
        on_delete=models.CASCADE, null=True
    )
    effect = models.ForeignKey(
        Effect, related_name='cards',
        on_delete=models.CASCADE, null=True
    )
    value = models.IntegerField()

    def __str__(self):
        if self.value > 0:
            string = '{} +{}'.format(self.effect, self.value)
        else:
            string = '{} {}'.format(self.effect, self.value)
        return string


class Character(models.Model):
    firstname = models.CharField(max_length=20)
    lastname = models.CharField(max_length=20)
    level = models.IntegerField(default=1)
    experience = models.IntegerField(default=0)
    image = models.ImageField(upload_to='character/', blank=True, null=True)
    strenght = models.IntegerField()
    dexterity = models.IntegerField()
    intellect = models.IntegerField()
    user = models.ForeignKey(
        User, related_name='characters',
        on_delete=models.CASCADE
    )

    def __str__(self):
        return '{} {}'.format(self.firstname, self.lastname)

    def get_hand(self):
        """ Returns the cards in character's hand """

        hand_query = self.cards.filter(on_hand__gt=0)
        hand = []
        for relative in hand_query:
            for _ in range(relative.on_hand):
                hand.append(relative.card)
        return hand

    def get_ground(self):
        """ Returns the cards in character's ground """

        ground_query = self.cards.filter(on_ground__gt=0)
        ground = []
        for relative in ground_query:
            for _ in range(relative.on_ground):
                ground.append(relative.card)
        return ground

    def get_pile(self):
        """ Returns the cards in character's pile """

        cards = self.cards.filter(on_pile__gt=0)
        if not cards:
            cards = self.cards.filter(on_discard__gt=0)
            for card in cards:
                card.on_pile = card.on_discard
                card.on_discard = 0
                card.save()
            cards = self.cards.filter(on_pile__gt=0)

        pile = []
        for card in cards:
            for _ in range(card.on_pile):
                pile.append(card)

        return pile

    def fill_ground(self, num=7):
        """ Fills character's ground with n random cards from pile """

        pile = self.get_pile()

        for _ in range(num):
            if not pile:
                pile = self.get_pile()
            card = pile.pop(randint(0, len(pile)-1))
            card.on_ground += 1
            card.on_pile -= 1
            card.save()

    def make_pile(self):
        pass

    def get_modifiers(self, option=None):
        """ Returns a dict of cumulated effects for cards in
            character's hand. """

        modifiers = {}
        for card in self.get_hand():
            for fx in card.effects.all():
                if fx.effect.name not in modifiers:
                    modifiers[fx.effect.name] = fx.value
                else:
                    modifiers[fx.effect.name] += fx.value

        return modifiers

    def move(self):
        """ Discards one card from ground and refill,
            discards all no enduring cards from character's hand """

        ground = self.get_ground()
        card = self.cards.get(card=choice(ground))
        card.on_ground -= 1
        card.on_discard += 1
        card.save()

        num = 8 - len(ground)
        if num > 0:
            self.fill_ground(num)
        hand = self.cards.filter(
            on_hand__gt=0).exclude(card__enduring=True)
        for card in hand:
            for _ in range(card.on_hand):
                card.on_discard += 1
            card.on_hand = 0
            card.save()


class CharacterCard(models.Model):
    character = models.ForeignKey(
        Character, related_name='cards',
        on_delete=models.CASCADE, null=True
    )
    card = models.ForeignKey(
        Card, related_name='characters',
        on_delete=models.CASCADE, null=True
    )
    on_ground = models.IntegerField(default=0)
    on_hand = models.IntegerField(default=0)
    on_pile = models.IntegerField(default=0)
    on_discard = models.IntegerField(default=0)
